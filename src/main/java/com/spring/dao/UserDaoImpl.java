package com.spring.dao;

import com.spring.model.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class UserDaoImpl  implements UserDao {
    @Autowired
    SessionFactory sessionFactory;
    @Override
    public void saveUser(User user) {
        sessionFactory.openSession().persist(user);
    }

    @Override
    public List<User> findAllUsers() {
        Criteria criteria = sessionFactory.openSession().createCriteria(User.class);
        return (List<User>)criteria.list();
    }

    @Override
    public void deleteUserById(int id) {
        NativeQuery query = sessionFactory.openSession().createSQLQuery("delete from t_user where id = :id_user");
        query.setParameter("id_user",id);
        query.executeUpdate();
    }

    @Override
    public User findById(int id) {
        Criteria criteria = sessionFactory.openSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id",id));
        return (User) criteria.uniqueResult();
    }

    @Override
    public void updateUser(User user) {
        sessionFactory.openSession().update(user);
    }
}
