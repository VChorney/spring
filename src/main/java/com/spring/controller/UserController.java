package com.spring.controller;

import com.spring.model.User;
import com.spring.service.UserService;
import com.spring.service.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    private UserService userService = new UserServiceImpl();
    @GetMapping("/users")
    List<User> all() {
        return userService.findAllUser();
    }

    @PostMapping("/users")
    void newEmployee(@RequestBody User user) {
         userService.saveUser(user);
    }
}
