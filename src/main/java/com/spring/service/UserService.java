package com.spring.service;

import com.spring.model.User;

import java.util.List;

public interface UserService {

    void saveUser(User user);

    List<User> findAllUser();

    void deleteUserById(int id);

    User findById(int id);

    void updateUser(User user);
}
